$("#keyword").keyup(function(){
    var ketikan = $("#keyword").val()
    console.log(ketikan)
    $.ajax({
        url: '/data?q=' + ketikan,
        success: function(data){
            var array_items = data.items;
            console.log(array_items)
            $("#daftar-buku").empty();
            if(array_items != null){
                $("#daftar-buku").append(
                    '<thead><tr><th>Sampul Buku</th><th>Judul</th><th>Penulis</th><th>Penerbit</th><th>Tanggal Terbit</th><th>ISBN</th></tr></thead><tbody>'
                );

                for(i=0;i<array_items.length;i++){
                    var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var judul = array_items[i].volumeInfo.title;
                    var link = array_items[i].volumeInfo.previewLink;
                    var penulis = array_items[i].volumeInfo.authors;
                    var penerbit = array_items[i].volumeInfo.publisher;
                    var tanggal = array_items[i].volumeInfo.publishedDate;
                    var isbn_array = array_items[i].volumeInfo.industryIdentifiers;
                    var isbn = isbn_array[0].identifier;
                    
    
                    if (gambar == null) {
                        gambar = "/static/img/M2-JS02-1.jpg";
                    }
                    if (penulis == null) {
                        penulis = "-";
                    }
                    if (penerbit == null) {
                        penerbit = "-";
                    }
                    if (tanggal == null) {
                        tanggal = "-";
                    }
                    if (isbn == null) {
                        isbn = "-";
                    }
    
                    $("#daftar-buku").append(
                        "<tr>" + 
                        "<td><img src=" + gambar + "</td>" + 
                        '<td><a style="text-decoration:none" href="' + link +  '">' + judul + "</a></td>" +
                        "<td>" + penulis + "</td>" +  
                        "<td>" + penerbit + "</td>" + 
                        "<td>" + tanggal + "</td>" + 
                        "<td>" + isbn + "</td>" + 
                        "</tr></tbody>"
                    );
                }
            }else{
                $("#daftar-buku").append(
                    "<p>Hasil tidak ditemukan<p>"
                );
            }
            
        }
    });
});