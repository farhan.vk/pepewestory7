from django.urls import path
from . import views
from django.contrib.auth import views as auth 

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signupView, name='signup'),
    path('login/', auth.LoginView.as_view(template_name='story9/login.html'), name='login'),
    path('logout/', auth.LogoutView.as_view(template_name='story9/logout.html'), name='logout'),
    path('profile/', views.profileView, name='profile'),

]
