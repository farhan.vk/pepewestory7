from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm

from .forms import RegisterForm, ProfileForm


# Create your views here.
def index(request):
    context = {
    }
    print(request.user.is_authenticated)
    

    return render(request, 'story9/index.html', context )

def signupView(request):
    if request.method == 'POST':
        form =RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, f'Successfully registered. You can now log in.')
            return redirect('story9:login')
    else:
        form = RegisterForm()

    context = {
        'form': form
    }
    return render(request, 'story9/signup.html', context)

@login_required
def profileView(request):   
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, f'Successfully updated profile.')
            return redirect('story9:profile')    
    else: 
        form = ProfileForm(instance=request.user.profile)

    context = {
        'form': form
    }
    return render(request, 'story9/profile.html', context)

