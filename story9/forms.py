from django import forms
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from .models import Profile

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password1',
            'password2',
        ]

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['user']
        widgets = {
            'image': forms.TextInput(
				attrs={
                    'class': 'form-control',
                    'id': 'image',
                    'placeholder': 'Image URL',
					}
				),
            'fname': forms.TextInput(
				attrs={
                    'class': 'form-control',
                    'id': 'fname',
                    'placeholder': 'First Name',
					}
				),
            'lname': forms.TextInput(
				attrs={
                    'class': 'form-control',
                    'id': 'lname',
                    'placeholder': 'Last Name',
					}
				),
        }
    
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['image'].label = "New profile picture"
        self.fields['fname'].label = "First Name"
        self.fields['lname'].label = "Last Name"