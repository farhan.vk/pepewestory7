from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json


def story8(request):
    response = {}
    return render(request, 'story8/story8.html', response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)
