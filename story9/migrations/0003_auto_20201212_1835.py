# Generated by Django 3.1.3 on 2020-12-12 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story9', '0002_auto_20201212_1821'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='fname',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='profile',
            name='image',
            field=models.CharField(default='https://www.kindpng.com/picc/m/22-223863_no-avatar-png-circle-transparent-png.png', max_length=100),
        ),
        migrations.AddField(
            model_name='profile',
            name='lname',
            field=models.CharField(default='', max_length=100),
        ),
    ]
